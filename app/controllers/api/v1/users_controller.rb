class Api::V1::UsersController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]

  def index
    @users = User.all
    render json: @users, only: [:name]
  end
end
